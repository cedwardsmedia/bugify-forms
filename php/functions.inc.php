<?php
require_once("config.inc.php");

// This function sets up our basic cURL GET action. We can use this later in custom functions without rewriting the same code.
function curl_get($resource) {
    
        $ch = curl_init(); // Start cURL session
        curl_setopt($ch, CURLOPT_URL, API_URL . "/api/" . $resource . ".json"); // Create our URL to cURL and request the JSON-encoded response.
        curl_setopt($ch, CURLOPT_POSTFIELDS, API_KEY); // Tell cURL to POST the API key.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = trim(curl_exec($ch)); // Set the cURL response to $response
        curl_close($ch); // Close cURL session
        $result = json_decode($response, true, 512);
        var_dump($result);
}

// This function will ping Bugify to get a list of projects and return them in an array.
function get_projects() {
    curl_get("projects");
}

// This function will ping Bugify to get the current version. It currently exists only to test JSON decoding and the API connection.
function get_version(){
        $ch = curl_init(); // Start cURL session
        curl_setopt($ch, CURLOPT_URL, API_URL . "/api/system" . ".json"); // Create our URL to cURL and request the JSON-encoded response.
        curl_setopt($ch, CURLOPT_POSTFIELDS, API_KEY); // Tell cURL to POST the API key.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = trim(curl_exec($ch)); // Set the cURL response to $response
        curl_close($ch); // Close cURL session
        $result = json_decode($response, true, 512);
        return $result["version"];
}
?>
