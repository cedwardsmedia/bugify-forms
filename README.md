# Bugify Forms
Created Oct 27, 2013  
Updated Nov 1, 2013

## Description

Bugify Forms will provide a basic PHP framework for communicating with a Bugify (<http://www.bugify.com>) installation to submit issues from a public-facing form.
Currently, the example forms will be built using Twitter Bootstrap, glued with the PHP backend to talk with Bugify.

## To Do

- create initial JavaScript widget
- plan anonymous feedback process (collect contact information, follow up ideally in issue threads)
- add ability to post screenshot along with issue report
- add some basic $_POST and junk filtering
- capture basic system/browser information where possible
- retrieve basic project information for forms

### Credits
Developed by:  
Matt Stein <http://www.workingconcept.com>  
Corey Edwards <http://www.cedwardsmedia.com>

Portions © 1997-2013 Corey Edwards All Rights Reserved.
### License
All Developer Previews are Licensed under the Corey Edwards Public Source License: <http://www.cedwardsmedia.com/cepsl> - Public Release license subject to change.

### Support
NOTICE: Bugify Forms is currently in early development and is NOT suitable for production use. Therefore, support is unavailable and all support requests will be ignored.

